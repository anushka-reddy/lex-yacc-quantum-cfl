%{
#include <stdio.h>
#include <stdlib.h>
enum {
    I,
    H,
    X,
    Z,
    CNOT,
    TOF,
    LPAREN,
    RPAREN,
    ASTERISK,
    KRONECKERPROD,
    NEWLINE
};
%}

%%

"I"                 { printf("Token: I; Lexeme: I\n"); }
"H"                 { printf("Token: H; Lexeme: H\n"); }
"X"                 { printf("Token: X; Lexeme: X\n"); }
"Z"                 { printf("Token: Z; Lexeme: Z\n"); }
"CNOT"              { printf("Token: CNOT; Lexeme: CNOT\n"); }
"TOF"               { printf("Token: TOF; Lexeme: TOF\n"); }
"\("                { printf("Token and Lexeme: (\n"); }
"\)"                { printf("Token and Lexeme: )\n"); }
"\*"                { printf("Token and Lexeme: *\n"); }
"\(x\)"             { printf("Token: KRONECKERPROD; Lexeme: (x)\n"); }
[ \t]+              { /* ignore whitespace */ }
\n                  { printf("Token and Lexeme: <newline>\n"); }
.                   { printf("Unrecognized character: %s.\n", yytext); }

%%

void yyerror(char *s) {
    fprintf(stderr, "Error: %s\n", s);
}

int yywrap(void) {
    return 1;
}

int main(void) {
    while (yylex() != 0);
    return 0;
}


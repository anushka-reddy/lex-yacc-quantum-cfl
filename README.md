### LexYaccQuantumCFL Readme

This readme provides an overview of the solutions for the tasks outlined in the specifications.

#### Problem 1: Constructing a Parser for PLUS-TIMES-POWER Language
- Implemented `prob1.l` as described to work with `plus-times-power.y`.
- The solution allows building a parser for the PLUS-TIMES-POWER language.

#### Problem 2: Context-Free Grammar for the QCIRC Language
- Developed a Context-Free Grammar (CFG) for the QCIRC language over the given alphabet.
- The grammar is presented in `prob2.pdf` and ensures adherence to the language's syntax rules.

#### Problem 3: Lexical Analyzer for QCIRC
- Created `prob3.l` to serve as a lexical analyzer for QCIRC.
- Defined simple regular expressions for various tokens and enabled parsing of QCIRC expressions.

#### Problem 4: Parser for QCIRC
- Modified `prob4.l` to work with Yacc and constructed `prob4.y` from `plus-times-power.y`.
- Implemented grammar rules and associated actions in Yacc format to evaluate QCIRC expressions.
- The parser utilizes predefined quantum expression functions and handles matrix operations.

#### Problem 5: Parser for QUANT Language
- Developed `prob5.l` and `prob5.y` based on the previous parser to handle QUANT language expressions.
- Extended the grammar rules to include KET0 and KET1 tokens and implemented associated actions.

#### Problem 6: Quantum Register Expression Conversion
- Utilized `prob6.awk` to convert the ID number to a quantum register expression.
- The generated expression, is saved in `prob6.txt`.
- Ran the parser on the expression to evaluate it and appended the output to `prob6.txt`.

#### Problem 7: Turing Machine for Pauli Product Word Simplification
- Built a Turing machine in Tuatara to compute the Pauli product word simplification function.
- Saved the Turing machine as `prob7.tm` to facilitate further analysis and testing.

#### Problem 8: Context-Free Language Analysis
- Investigated the language SAW of strings representing self-avoiding walks.
- Analyzed whether SAW is context-free, considering the Cocke-Younger-Kasami algorithm.
- Presented the analysis and conclusion in `prob8.pdf`.
